const images = [
    '1.jpg',
    '2.jpg',
    '3.jpg',
    '4.jpeg',
    '5.jpg',
    '6.jpg'
];

var currentIndex = 0;                                      //начальный индекс картинки
const slider1 = document.getElementsByClassName('slider1-img')[0];
var s = document.getElementById('slider1-prev');
s.addEventListener('click', slide1Prev);

document.getElementById('slider1-next').addEventListener('click', slide1Next);



renderImage();

function renderImage() {
    slider1.src = './images/' + images[currentIndex];
}

function slide1Next() {
    if (currentIndex < images.length - 1)
        currentIndex++;
    else
        currentIndex = 0;

    renderImage();
}

function slide1Prev() {
    if (currentIndex > 0)
        currentIndex--;

    renderImage();
}
//------------второй слайдер-------------------------    

renderAll();
var current2Index = 0;

document.getElementById('slider2-prev').addEventListener('click', slide2Prev);
document.getElementById('slider2-next').addEventListener('click', slide2Next);

const allImages = document.getElementsByClassName('slider2-img'); // массив из всех картинок
showImage();

function renderAll() {
    const group = document.getElementsByClassName('slider2-group')[0];
    for (var i = 0; i < images.length; i++) {
        const img = document.createElement('img');           //метод создания тега. в скобках его имя. img, div и т.д;
        img.src = './images/' + images[i];
        img.classList.add('slider2-img');                    //обращение ко всему списку классов элемента;
        //.remove('удаляемый класс');.add('добавляемый класс');
        group.appendChild(img);                              //метод добавления элемента внутрь себя;
    }
}
function showImage() {
    allImages[current2Index].style.opacity =1;
}
function slide2Prev() {
    if (current2Index > 0) {
        allImages[current2Index].style.opacity = 0;
        current2Index--;
        allImages[current2Index].style.opacity = 1;
    } else {
        allImages[current2Index].style.opacity = 0;
        current2Index = images.length - 1;
        allImages[current2Index].style.opacity = 1;
    }
}
function slide2Next() {
    if (current2Index < images.length - 1){
    currentIndex++;
        allImages[current2Index].style.opacity = 0;
        current2Index++;
        allImages[current2Index].style.opacity = 1;
    } else {
        allImages[current2Index].style.opacity = 0;
        current2Index = 0;
        allImages[current2Index].style.opacity = 1;
    }
}
//------------------slider 3 ------------------------------------

var current3Index = 0;
document.getElementById('slider3-prev').addEventListener('click', slide3Prev);
document.getElementById('slider3-next').addEventListener('click', slide3Next);
function slide3Prev(){
    if(current3Index > 0){
        current3Index--;
        const size = document.getElementsByClassName('slider3-img')[1].getBoundingClientRect(); 
        const offset = current3Index * size.width;
       
                                    //getBoundingClientRect() функция получающая все размеры
        document.getElementsByClassName('line')[0].style.transform = `translateX(${-offset}px)`;
    }
}
function slide3Next(){
    if(current3Index < images.length-1){
        current3Index++;
        const offset = current3Index * 200;    //размер картинки
        document.getElementsByClassName('line')[0].style.transform = `translateX(${-offset}px)`;
    }
}


//http://kenwheeler.github.io/slick/  библиотека слайдеров
// $('[src]').on('click', .....); если подключен jquerry
// document.getElementsByClassName('line')[0].addEventListener('touchmove', swipe);

// function swipe(event){
//     console.log(event);
// }
//console.log($('.line'));
$('.slickslide').slick({
    
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: true
      
});