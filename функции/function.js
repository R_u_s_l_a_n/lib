// <?php 
// if(isset($_POST['send'])){
// 	$text=$_POST['text']
// }
// mail("pochta@mail.ru", "автоматическое письмо", $text);
// header(location: /);?>
//-----------------функция добавления класса в класс для выпадающего меню----------------------------
function MyFunction(){
    var x = document.getElementById('myTopnav');
    if(x.localName === "topnav"){
        x.className+="_responsive";
    } else{
        x.className = "topnav";
    }
}
//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----
var square = function(x){
    return x*x;
}
//document.write(square(12) + "<br>");
//console.log(square(12)); 
//---------------------------------------------------------------------------------------------------
//-----------------разделение строки на массив и обратное соединение---------------------------------
var text ='мама.мыла.и.раму';
text = text.split('.');
//console.log(text);

var newText = '';
text.forEach(item=>{
    newText += ' ' + item;
});
//console.log(newText);
//---------------------------------------------------------------------------------------------------
//-----------------рандомное число от 0 до 1---------------------------------------------------------
Math.random();
//console.log(Math.random());
//---------------------------------------------------------------------------------------------------
//-----------------функция рандомного значения в интервале from -> to--------------------------------
function getRandom(from, to){
    var rand = from - 0.5 + Math.random()*(to - from +1);
    // rand = -0.5 + Math.random()*(to + 1); рандом только положительных чисел;
    return Math.round(rand);
}
//console.log(getRandom(-10, 50));
//---------------------------------------------------------------------------------------------------
//-----------------функция по смене цвета фона в момент движения мыши--------------------------------
const changeColor = document.getElementById('demo');
//changeColor.addEventListener('mousemove', chColor); коммент-ошибки
function chColor(){   
         s = randColor(); 
         changeColor.style.backgroundColor = changeColor.innerHTML= s; 
         changeColor.style.transition='.2s';
         //changeColor.innerHTML = s;  //показывает номер цвета;
         
}
function randColor() {
    var r = Math.floor(Math.random() * (256)),
        g = Math.floor(Math.random() * (256)),
        b = Math.floor(Math.random() * (256));
    return '#' + r.toString(16) + g.toString(16) + b.toString(16); 
                     //метод .toString(16) преобразует в 16-ю систему счисления
} 

//---------------------------------------------------------------------------------------------------
//-----------------всякие варианты работы с датой и временем-----------------------------------------
//console.log(date.getTime());
//console.log(date);
///const  day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
//const year = date.getFullYear();
//const month = date.getMonth() + 1 < 10 ? `0${date.getMonth()+1}` : date.getMonth();
//const hor = date.getHours() < 10 ? `0${date.getHours()}` : date.getHours();
//const min = date.getMinutes()< 10 ? `0${date.getMinutes()}` : date.getMinutes();
//const sec = date.getSeconds()< 10 ? `0${date.getSeconds()}` : date.getSeconds();
//const fulldate = day + '-' + month + '-' + year + ' ' + hor + ':' + min + ':' + sec;
///const fulldate = `${day}-${month}-${year} ${hor}:${min}:${sec}`;
//console.log(fulldate);
/*const  day = getTrueNumbers(date.getDate());
const  month = getTrueNumbers(date.getMonth() + 1);
const  hor = getTrueNumbers(date.getHours());
const  min = getTrueNumbers(date.getMinutes());
const  sec = getTrueNumbers(date.getSeconds());
const  year = getTrueNumbers(date.getFullYear());
function getTrueNumbers(number){
return number < 10 ? `0${number}` : number;
}
//const fulldate = `${day}-${month}-${year} ${hor}:${min}:${sec}`;
//console.log(fulldate);

function getFullDate(){
    var date = new Date();
    const  day = getTrueNumbers(date.getDate());
const  month = getTrueNumbers(date.getMonth() + 1);
const  hor = getTrueNumbers(date.getHours());
const  min = getTrueNumbers(date.getMinutes());
const  sec = getTrueNumbers(date.getSeconds());
const  year = getTrueNumbers(date.getFullYear());
function getTrueNumbers(number){
return number < 10 ? `0${number}` : number;
}
const fulldate = `${day}-${month}-${year} ${hor}:${min}:${sec}`;
console.log(fulldate);
}
const interval = setInterval(getFullDate, 1000); //миллисекунды;
setTimeout(() => {
    console.log('Stop interval');
    clearInterval(interval);
}, 5000);

//console.log(moment().unix());
//console.log(moment().format('MM-YYYY'));
//moment.locale('ru'); //перевод на язык;
//console.log(moment.unix(55555555).utcOffset('+0400').format('ll')); 
//перевод времени из unix; utcOffset('+0400')-часовой пояс;*/

//---------------------------------------------------------------------------------------------------
//-----------------функция меняющая цвет блока при нажатии на него с зеленого на красный и наоборот----
const demo = document.getElementById('demo');

//demo.addEventListener('click', setRed);     коммент-ошибки
f = true;
function setRed(event){ 
    var item = event.target;
    if(f){item.style.backgroundColor = 'green';
}
    else {item.style.backgroundColor = 'red';
}
f = !f;
}

//demo.addEventListener('mouseenter', setRed);   при наведении и отведениии работает функция setRed();
//demo.addEventListener('mouseleave', setRed);
//---------------------------------------------------------------------------------------------------
//-----------------скрипт по перемещению объекта по браузерному окну---------------------------------

dragElement(document.getElementById(("mydiv")));

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    /* if present, the header is where you move the DIV from:*/
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    /* otherwise, move the DIV from anywhere inside the DIV:*/
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

//---------------------------------------------------------------------------------------------------
//-----------------функция устанвки бесконечного таймера---------------------------------------------
var counter =0;
	function timef(){
            
            document.getElementById('demos').innerHTML = counter;
            counter++;
            setTimeout(timef, 100);             
    }  
    // timef();
    
    
//---------------------------------------------------------------------------------------------------
//-----------------функция суммы двух элементов из полей форм----------------------------------------
function summago() {
    var a = document.getElementById('a').value;
    var b = document.getElementById('b').value;
    /*a= parseInt(a);
    b= parseInt(b);
    c= parseInt(c);*/
    var c = +a+ +b;
    document.getElementById('summa').innerHTML = c;
}

//---------------------------------------------------------------------------------------------------
//-----------------ПЕРЕМЕЩЕНИЕ КВАДРАТИКА В ПОЛЕ-----------------------------------------------------
function move(e){	/* для вызова какого либо события в зависимости от нажатия 
	конкретной клавиши   http://keycode.info/
	if (e.keyCode==13) {     
alert("вы нажали enter");
	}

*/

		var blueRect = document.getElementById("blueRect");
		// получаем стиль для blueRect;
		var cs = window.getComputedStyle(blueRect);
		var left = parseInt(cs.marginLeft);
		var top = parseInt(cs.marginTop);


		switch(e.keyCode){
			case 40: 	
			if (top<400) {
				blueRect.style.marginTop = top + 10 + "px";
			}					//по коду из  http://keycode.info/;
			
			break;
			case 38:
			if(top>0) {						
			blueRect.style.marginTop = top - 10 + "px";
		}
			break;
			case 39: 
			if (left<700) {								
			blueRect.style.marginLeft = left + 10 + "px";
		}
			break;
			case 37: 
				if (left>0) {						
			blueRect.style.marginLeft = left - 10 + "px";
		}
			break;
		}
}
addEventListener("keydown", move);
//---------------------------------------------------------------------------------------------------
//-----------------время работы сайта----------------------------------------------------------------
d0 = new Date('April 4, 2017'); // пуск сайта
d1 = new Date();
dt = (d1.getTime() - d0.getTime()) / (1000*60*60*24);
document.write('Работает <B>' + Math.round(dt) + '</B> дней');


//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----

//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----


//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----

//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----


//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----

//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----


//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----

//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----


//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----

//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----


//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----

//---------------------------------------------------------------------------------------------------
//-----------------присвоение переменной функции и передача переменной параметров как для функции----


//---------------------------------------------------------------------------------------------------